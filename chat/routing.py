from django.urls import path
from django.conf.urls import url
from . import consumers

websocket_urlpatterns = [
	url(r'^ws/chat/chatrooms/(?P<room_uuid>[^/]+)/$',consumers.ChatConsumer),
	url(r'^ws/chat/users/(?P<username>[^/]+)/$',consumers.AlertConsumer),
]
