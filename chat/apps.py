from django.apps import AppConfig


class ChatterConfig(AppConfig):
    name = 'chat'
    verbose_name = "Django Chat"
